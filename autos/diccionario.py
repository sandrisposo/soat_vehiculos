from django.db import models
from django.db import JSONField

class ContactInfo(models.Model):
    data = models.JSONField()

ContactInfo.objects.create(data={
    'name': 'John',
    'cities': ['London', 'Cambridge'],
    'pets': {'dogs': ['Rufus', 'Meg']},
})
ContactInfo.objects.filter(
    data__name='John',
    data__pets__has_key='dogs',
    data__cities__contains='London',
).delete()

class MyModel(models.Model):
    json = JSONField()