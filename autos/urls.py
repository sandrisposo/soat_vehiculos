from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('', inicio, name='home'),
    path('buscar_vehiculo/', buscar_vehiculo)
    # path('login/', views.login, name='login'),
    # path('register/', views.register, name='register'),
]
