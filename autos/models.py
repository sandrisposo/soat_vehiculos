from typing import List
from django.db import models
# from django.contrib.postgres.fields import JSONField
from datetime import date

class Vehiculo(models.Model):
    id = models.AutoField(primary_key=True)
    info = models.JSONField()


    class Meta:
        verbose_name = 'Vehiculo'  
        verbose_name_plural = 'Vehiculos'


 