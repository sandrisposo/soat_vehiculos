from datetime import datetime, date
from distutils.log import info
from email import message
from django.contrib import messages
from django.shortcuts import render
from django.http import  HttpResponse
# from django.forms import Valueform
from autos.models import *

def inicio(request):
    print('inicio')
    if request.method == 'POST':
        # if request.POST.get('marca')!='' and request.POST.get('placa')!='' and request.POST.get('modelo')!='' and\
        # request.POST.get('tecnomecanica')!='' and request.POST.get('soat')!='' and\
        # request.POST.get('date_tecnomecanica')!='' and request.POST.get('date_soat'):
        try:
            marca =  request.POST.get('marca')
            placa = request.POST.get('placa')
            modelo = request.POST.get('modelo')
            tecnomecanica = request.POST.get('tecnomecanica')
            date_tecnomecanica = request.POST.get('date_tecnomecanica')
            soat = request.POST.get('soat')
            date_soat = request.POST.get('date_soat')

            info_vehiculo = {
                'marca':marca,
                'placa':placa,
                'modelo':modelo,
                'tecnomecanica':tecnomecanica,
                'date_tecnomecanica':date_tecnomecanica,
                'soat':soat,
                'date_soat':date_soat     
            }
            try:
                Vehiculo(info=info_vehiculo).save()
                messages.success(request, 'se registra vehiculo')
            except:
                messages.error(request, 'no se registra vehiculo')
        except:
            messages.error(request, 'ingresa todos los datos del vehiculo')

    return render(request, 'inicio.html')

def buscar_vehiculo(request):
    if 'placa' in request.GET:
        placa = request.GET['placa']

        try:
            ver = Vehiculo.objects.get(info__placa=f"{placa}")
            diccionario = ver.info
            fecha = datetime.strptime(diccionario['date_soat'], '%Y-%m-%d')
            if fecha.date() > date.today():
                messages.success(request, 'Su SOAT vence el '+ diccionario['date_soat'])    
            else:
                messages.error(request, 'Su SOAT esta vencido, su vencimiento fue el '+ diccionario['date_soat'])

        except:
            messages.error(request, 'el vehiculo no existe')

    return render(request, 'buscar_vehiculo.html')   



    